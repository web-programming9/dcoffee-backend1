import { Module } from '@nestjs/common';
import { BillsService } from './bills.service';
import { BillsController } from './bills.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bill } from './entities/bill.entity';
import { BillDetail } from 'src/bill_details/entities/bill_detail.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bill, BillDetail, Employee])],
  controllers: [BillsController],
  providers: [BillsService],
})
export class BillsModule {}
