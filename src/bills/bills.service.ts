import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BillDetail } from 'src/bill_details/entities/bill_detail.entity';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private billsRepository: Repository<Bill>,
    @InjectRepository(BillDetail)
    private billDetailsRepository: Repository<BillDetail>,
  ) {}

  async create(createBillDto: CreateBillDto) {
    const billDetails = await this.billDetailsRepository.findOne({
      where: {
        id: createBillDto.bill_detailId,
      },
    });
    const newBill = new Bill();
    newBill.shop_name = createBillDto.shop_name;
    newBill.date_time = createBillDto.date_time;
    newBill.total = createBillDto.total;
    newBill.buy = createBillDto.buy;
    newBill.change = createBillDto.change;
    newBill.bill_details = billDetails;
    await this.billsRepository.save(newBill);

    for (const od of createBillDto.bill_detailItem) {
      const bill_detail = new BillDetail();
      bill_detail.amount = od.amount;
      bill_detail.name = bill_detail.name;
      bill_detail.price = bill_detail.price;
      bill_detail.total = bill_detail.price * bill_detail.amount;

      await this.billDetailsRepository.save(bill_detail);
      newBill.buy = newBill.buy + bill_detail.amount;
      newBill.total = newBill.total + bill_detail.total;
    }
    await this.billDetailsRepository.save(newBill);
    return await this.billDetailsRepository.findOne({
      where: { id: newBill.id },
      relations: ['bill_details'],
    });
  }

  findAll() {
    return this.billsRepository.find({ relations: ['bill_details'] });
  }

  findOne(id: number) {
    return this.billsRepository.findOne({
      where: { id: id },
      relations: ['bill_details'],
    });
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    const bill = await this.billsRepository.findOneBy({
      id: id,
    });
    if (!bill) {
      throw new NotFoundException();
    }
    const updateBill = { ...bill, ...updateBillDto };
    return this.billsRepository.save(updateBill);
  }

  async remove(id: number) {
    const Bill = await this.billsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedBill = await this.billsRepository.remove(Bill);
      return deletedBill;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
