import { IsNotEmpty, Length, Min } from 'class-validator';
import { CreateBillDetailDto } from 'src/bill_details/dto/create-bill_detail.dto';
export class CreateBillDto {
  @IsNotEmpty()
  @Length(3, 32)
  shop_name: string;

  @IsNotEmpty()
  date_time: Date;

  @IsNotEmpty()
  @Min(0)
  total: number;

  @IsNotEmpty()
  @Min(0)
  buy: number;

  @IsNotEmpty()
  @Min(0)
  change: number;

  @IsNotEmpty()
  bill_detailId: number;
  bill_detailItem: CreateBillDetailDto[];
}
