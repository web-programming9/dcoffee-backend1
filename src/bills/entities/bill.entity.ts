import { BillDetail } from 'src/bill_details/entities/bill_detail.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
  Entity,
} from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  shop_name: string;

  @CreateDateColumn()
  date_time: Date;

  @Column({ type: 'float' })
  total: number;

  @Column({ type: 'float' })
  buy: number;

  @Column({ type: 'float' })
  change: number;

  @ManyToOne(() => Employee, (employee) => employee.bills)
  employee: Employee;

  @OneToMany(() => BillDetail, (bill_details) => bill_details.bill)
  bill_details: BillDetail;

  @UpdateDateColumn()
  updatedAt: Date;
}
