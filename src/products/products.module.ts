import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Catagory } from 'src/catagorys/entities/catagory.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import { Order } from 'src/orders/entities/order.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Product, Catagory, OrderItem, Order])],
  controllers: [ProductsController],
  providers: [ProductsService],
})
export class ProductsModule {}
