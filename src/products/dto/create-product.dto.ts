import { IsNotEmpty, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  // @IsNotEmpty()
  @Length(1, 32)
  type: string;

  // @IsNotEmpty()
  @Length(1, 32)
  size: string;

  @IsNotEmpty()
  // @Min(0)
  price: number;

  image = 'no-product-image.png';

  @IsNotEmpty()
  // @Min(0)
  catagoryId: number;
}
