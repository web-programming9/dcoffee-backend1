import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Catagory } from 'src/catagorys/entities/catagory.entity';
import { Like, Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Catagory)
    private catagorysRepository: Repository<Catagory>,
  ) {}

  async create(createProductDto: CreateProductDto) {
    const catagory = await this.catagorysRepository.findOne({
      where: {
        id: createProductDto.catagoryId,
      },
    });
    const newProduct = new Product();
    newProduct.name = createProductDto.name;
    newProduct.type = createProductDto.type;
    newProduct.size = createProductDto.size;
    newProduct.price = createProductDto.price;
    newProduct.image = createProductDto.image;
    newProduct.catagory = catagory;
    return this.productsRepository.save(newProduct);
  }

  // findByCatagory(id: number) {
  //   return this.productsRepository.find({ where: { id: id } });
  // }

  async findByCatagory(id: number) {
    const products = await this.productsRepository.find({
      where: {
        catagory: { id: id },
      },
    });
    return products;
  }

  async findAll(query): Promise<Paginate> {
    const page = query.page || 1;
    const take = query.take || 10;
    const skip = (page - 1) * take;
    const keyword = query.keyword || '';
    const orderBy = query.orderBy || 'name';
    const order = query.order || 'ASC';
    const currentPage = page;
    const [result, total] = await this.productsRepository.findAndCount({
      where: { name: Like(`%${keyword}%`) },
      order: { [orderBy]: order },
      take: take,
      skip: skip,
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage,
    };
  }

  findOne(id: number) {
    return this.productsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    try {
      const updatedProduct = await this.productsRepository.save({
        id,
        ...updateProductDto,
      });
      return updatedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedProduct = await this.productsRepository.remove(product);
      return deletedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
