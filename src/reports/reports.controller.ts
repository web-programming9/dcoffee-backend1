import { Controller, Get } from '@nestjs/common';
import { ReportsService } from './reports.service';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @Get('/customer')
  getCustomer() {
    return this.reportsService.getCustomer();
  }

  @Get('/product')
  getProduct() {
    return this.reportsService.getProduct();
  }

  @Get('/user')
  getUser() {
    return this.reportsService.getUser();
  }

  @Get('/employee')
  getEmployee() {
    return this.reportsService.getEmployee();
  }

  @Get('/order_item')
  getOrder_item() {
    return this.reportsService.getOrder_item();
  }

  @Get('/materialdetail')
  getMaterial() {
    return this.reportsService.getMaterial();
  }

  @Get('/delcus')
  delCus() {
    return this.reportsService.delCus();
  }
}
