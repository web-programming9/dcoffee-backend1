import { Injectable } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportsService {
  constructor(@InjectDataSource() private dataSource: DataSource) {}

  getCustomer() {
    return this.dataSource.query('SELECT * FROM getCustomer');
  }

  getProduct() {
    return this.dataSource.query('SELECT * FROM getProduct');
  }

  getUser() {
    return this.dataSource.query('SELECT * FROM getUser');
  }

  getEmployee() {
    return this.dataSource.query('SELECT * FROM getEmployee');
  }

  getOrder_item() {
    return this.dataSource.query('SELECT * FROM getOrder_item');
  }

  getMaterial() {
    return this.dataSource.query('SELECT * FROM getMaterial');
  }

  delCus() {
    return this.dataSource.query('CALL `delCus`');
  }
}
