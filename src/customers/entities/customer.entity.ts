import { Order } from 'src/orders/entities/order.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '50',
  })
  name: string;

  @Column({
    length: '10',
    default: '-',
    unique: true,
  })
  tel: string;

  @Column({
    default: 0,
  })
  point: number;

  @CreateDateColumn()
  start_date: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @OneToMany(() => Order, (order) => order.customer)
  orders: Order[];
}
