import { IsNotEmpty, Length } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  @Length(10, 10)
  tel: string;

  @IsNotEmpty()
  point: number;
}
