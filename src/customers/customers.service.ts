import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto) {
    return this.customersRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customersRepository.find({});
  }

  async findOne(id: number) {
    const customer = await this.customersRepository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }
  async getCustomerByTel(tel: string): Promise<Customer[]> {
    return await this.customersRepository.find({
      where: { tel: tel },
    });
  }

  async addPointsToCustomerByTel(
    tel: string,
    pointsToAdd: number,
  ): Promise<Customer> {
    const customer = await this.getCustomerByTel(tel);

    if (customer.length === 0) {
      throw new NotFoundException('Customer not found');
    }

    const updatedCustomer = customer[0];
    updatedCustomer.point += pointsToAdd;
    return await this.customersRepository.save(updatedCustomer);
  }
  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customersRepository.findOneBy({
      id: id,
    });
    if (!customer) {
      throw new NotFoundException();
    }
    const updatedCustomer = { ...customer, ...updateCustomerDto };
    return this.customersRepository.save(updatedCustomer);
  }

  async remove(id: number) {
    const customer = await this.customersRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedcustomer = await this.customersRepository.remove(customer);
      return deletedcustomer;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
