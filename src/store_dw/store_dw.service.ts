import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateStoreDwDto } from './dto/create-store_dw.dto';
import { UpdateStoreDwDto } from './dto/update-store_dw.dto';
import { StoreDw } from './entities/store_dw.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StoreDwService {
  constructor(
    @InjectRepository(StoreDw)
    private storesDwRepository: Repository<StoreDw>,
  ) {}

  create(createStoreDwDto: CreateStoreDwDto) {
    return this.storesDwRepository.save(createStoreDwDto);
  }

  findAll() {
    return this.storesDwRepository.find({});
  }

  findOne(Store_Key: number) {
    return this.storesDwRepository.findOne({ where: { Store_Key } });
  }

  async update(Store_Key: number, updateStoreDwDto: UpdateStoreDwDto) {
    const storeDw = await this.storesDwRepository.findOneBy({ Store_Key });
    if (!storeDw) {
      throw new NotFoundException();
    }
    const updatedStoreDw = { ...storeDw, ...updateStoreDwDto };
    return this.storesDwRepository.save(updatedStoreDw);
  }

  async remove(Store_Key: number) {
    const storeDw = await this.storesDwRepository.findOne({
      where: { Store_Key: Store_Key },
    });
    try {
      const deletedSotreDw = await this.storesDwRepository.remove(storeDw);
      return deletedSotreDw;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
