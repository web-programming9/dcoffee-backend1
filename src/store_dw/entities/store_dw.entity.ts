import { Dashboard } from 'src/dashboard/entities/dashboard.entity';
import { PrimaryColumn, Column, OneToMany, Entity } from 'typeorm';

@Entity()
export class StoreDw {
  @PrimaryColumn()
  Store_Key: number;

  @Column()
  Store_DW_Name: string;

  @Column()
  Store_DW_Province: string;

  @Column()
  Store_DW_District: string;

  @Column()
  Store_DW_SubDistrict: string;

  @Column()
  Store_DW_Zipcode: string;

  @OneToMany(() => Dashboard, (Dashboard) => Dashboard.Store_Key)
  Dashboard: Dashboard;
}
