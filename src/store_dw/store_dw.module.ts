import { Module } from '@nestjs/common';
import { StoreDwService } from './store_dw.service';
import { StoreDwController } from './store_dw.controller';
import { StoreDw } from './entities/store_dw.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([StoreDw])],
  controllers: [StoreDwController],
  providers: [StoreDwService],
})
export class StoreDwModule {}
