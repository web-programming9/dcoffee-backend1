import { Employee } from 'src/employees/entities/employee.entity';
import { SummarySalary } from 'src/summary_salarys/entities/summary_salary.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CheckinOut {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  date: Date;

  @CreateDateColumn()
  time_in: Date;

  @Column()
  time_out: Date;

  @Column({ default: 0 })
  total_hour: number;

  @ManyToOne(() => Employee, (employee) => employee.checkinOut)
  employee: Employee;

  @ManyToOne(() => SummarySalary, (summary_salary) => summary_salary.checkinOut)
  summary: SummarySalary;

  @UpdateDateColumn()
  updatedDate: Date;
}
