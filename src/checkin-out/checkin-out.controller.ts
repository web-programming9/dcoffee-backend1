import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckinOutService } from './checkin-out.service';
import { CreateCheckinOutDto } from './dto/create-checkin-out.dto';
import { UpdateCheckinOutDto } from './dto/update-checkin-out.dto';

@Controller('checkin-out')
export class CheckinOutController {
  constructor(private readonly checkinOutService: CheckinOutService) {}

  @Post()
  create(@Body() createCheckinOutDto: CreateCheckinOutDto) {
    return this.checkinOutService.create(createCheckinOutDto);
  }

  @Get()
  findAll() {
    return this.checkinOutService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkinOutService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckinOutDto: UpdateCheckinOutDto,
  ) {
    return this.checkinOutService.update(+id, updateCheckinOutDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkinOutService.remove(+id);
  }
}
