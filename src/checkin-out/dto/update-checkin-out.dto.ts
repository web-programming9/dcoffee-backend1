import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckinOutDto } from './create-checkin-out.dto';

export class UpdateCheckinOutDto extends PartialType(CreateCheckinOutDto) {}
