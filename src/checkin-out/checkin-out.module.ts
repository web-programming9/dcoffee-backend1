import { Module } from '@nestjs/common';
import { CheckinOutService } from './checkin-out.service';
import { CheckinOutController } from './checkin-out.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckinOut } from './entities/checkin-out.entity';
import { SummarySalary } from 'src/summary_salarys/entities/summary_salary.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckinOut, Employee, SummarySalary])],
  controllers: [CheckinOutController],
  providers: [CheckinOutService],
})
export class CheckinOutModule {}
