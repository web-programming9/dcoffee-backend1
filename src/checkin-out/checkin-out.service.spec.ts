import { Test, TestingModule } from '@nestjs/testing';
import { CheckinOutService } from './checkin-out.service';

describe('CheckinOutService', () => {
  let service: CheckinOutService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckinOutService],
    }).compile();

    service = module.get<CheckinOutService>(CheckinOutService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
