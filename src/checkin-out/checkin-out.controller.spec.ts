import { Test, TestingModule } from '@nestjs/testing';
import { CheckinOutController } from './checkin-out.controller';
import { CheckinOutService } from './checkin-out.service';

describe('CheckinOutController', () => {
  let controller: CheckinOutController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckinOutController],
      providers: [CheckinOutService],
    }).compile();

    controller = module.get<CheckinOutController>(CheckinOutController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
