import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckinOutDto } from './dto/create-checkin-out.dto';
import { UpdateCheckinOutDto } from './dto/update-checkin-out.dto';
import { CheckinOut } from './entities/checkin-out.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CheckinOutService {
  constructor(
    @InjectRepository(CheckinOut)
    private checkinOutsRepository: Repository<CheckinOut>,
  ) {}

  async create(createCheckinOutDto: CreateCheckinOutDto) {
    return this.checkinOutsRepository.save(createCheckinOutDto);
  }

  findAll() {
    return this.checkinOutsRepository.find({
      relations: ['employee', 'summary'],
    });
  }

  findOne(id: number) {
    return this.checkinOutsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateCheckinOutDto: UpdateCheckinOutDto) {
    const checkinOut = await this.checkinOutsRepository.findOneBy({
      id: id,
    });
    if (!checkinOut) {
      throw new NotFoundException();
    }
    const updateSummarySalary = { ...checkinOut, ...updateCheckinOutDto };
    return this.checkinOutsRepository.save(updateSummarySalary);
  }

  async remove(id: number) {
    const checkinOut = await this.checkinOutsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedcheckinOut = await this.checkinOutsRepository.remove(
        checkinOut,
      );
      return deletedcheckinOut;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
