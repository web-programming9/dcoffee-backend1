import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCatagoryDto } from './dto/create-catagory.dto';
import { UpdateCatagoryDto } from './dto/update-catagory.dto';
import { Catagory } from './entities/catagory.entity';

@Injectable()
export class CatagorysService {
  constructor(
    @InjectRepository(Catagory)
    private catagorysRepository: Repository<Catagory>,
  ) {}
  create(createCatagoryDto: CreateCatagoryDto) {
    return this.catagorysRepository.save(createCatagoryDto);
  }

  findAll() {
    return this.catagorysRepository.find({});
  }

  findOne(id: number) {
    return this.catagorysRepository.findOne({ where: { id } });
  }

  async update(id: number, updateCatagoryDto: UpdateCatagoryDto) {
    const catagory = await this.catagorysRepository.findOneBy({ id });
    if (!catagory) {
      throw new NotFoundException();
    }
    const updateCatagory = { ...catagory, ...updateCatagoryDto };
    return this.catagorysRepository.save(updateCatagory);
  }

  async remove(id: number) {
    const catagory = await this.catagorysRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedcatagory = await this.catagorysRepository.remove(catagory);
      return deletedcatagory;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
