import { PartialType } from '@nestjs/mapped-types';
import { CreateEmployeeDwDto } from './create-employee_dw.dto';

export class UpdateEmployeeDwDto extends PartialType(CreateEmployeeDwDto) {}
