import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateEmployeeDwDto } from './dto/create-employee_dw.dto';
import { UpdateEmployeeDwDto } from './dto/update-employee_dw.dto';
import { EmployeeDw } from './entities/employee_dw.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class EmployeeDwService {
  constructor(
    @InjectRepository(EmployeeDw)
    private EmployeeDwsitory: Repository<EmployeeDw>,
  ) {}

  create(createEmployeeDwDto: CreateEmployeeDwDto) {
    return this.EmployeeDwsitory.save(createEmployeeDwDto);
  }

  findAll() {
    return this.EmployeeDwsitory.find({});
  }

  findOne(Employee_Key: number) {
    return this.EmployeeDwsitory.findOne({ where: { Employee_Key } });
  }

  async update(Employee_Key: number, updateEmployeeDwDto: UpdateEmployeeDwDto) {
    const EmployeeDw = await this.EmployeeDwsitory.findOneBy({ Employee_Key });
    if (!EmployeeDw) {
      throw new NotFoundException();
    }
    const updatedEmployeeDw = { ...EmployeeDw, ...updateEmployeeDwDto };
    return this.EmployeeDwsitory.save(updatedEmployeeDw);
  }

  async remove(Employee_Key: number) {
    const EmployeeDw = await this.EmployeeDwsitory.findOne({
      where: { Employee_Key: Employee_Key },
    });
    try {
      const deletedEmployeeDw = await this.EmployeeDwsitory.remove(EmployeeDw);
      return deletedEmployeeDw;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
