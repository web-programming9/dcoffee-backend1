import { Dashboard } from 'src/dashboard/entities/dashboard.entity';
import { Column, OneToMany, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class EmployeeDw {
  @PrimaryColumn()
  Employee_Key: number;

  @Column()
  Employee_DW_Name: string;

  @OneToMany(() => Dashboard, (Dashboard) => Dashboard.Employee_Key)
  Dashboard: Dashboard;
}
