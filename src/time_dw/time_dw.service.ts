import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTimeDwDto } from './dto/create-time_dw.dto';
import { UpdateTimeDwDto } from './dto/update-time_dw.dto';
import { TimeDw } from './entities/time_dw.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TimeDwService {
  constructor(
    @InjectRepository(TimeDw)
    private timeDwsitory: Repository<TimeDw>,
  ) {}

  create(createTimeDwDto: CreateTimeDwDto) {
    return this.timeDwsitory.save(createTimeDwDto);
  }

  findAll() {
    return this.timeDwsitory.find({});
  }

  findOne(Time_Key: number) {
    return this.timeDwsitory.findOne({ where: { Time_Key } });
  }

  async update(Time_Key: number, updateTimeDwDto: UpdateTimeDwDto) {
    const TimeDw = await this.timeDwsitory.findOneBy({ Time_Key });
    if (!TimeDw) {
      throw new NotFoundException();
    }
    const updatedTimeDw = { ...TimeDw, ...updateTimeDwDto };
    return this.timeDwsitory.save(updatedTimeDw);
  }

  async remove(Time_Key: number) {
    const TiemDw = await this.timeDwsitory.findOne({
      where: { Time_Key: Time_Key },
    });
    try {
      const deletedTimeDw = await this.timeDwsitory.remove(TiemDw);
      return deletedTimeDw;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
