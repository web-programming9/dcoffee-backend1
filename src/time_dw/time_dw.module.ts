import { Module } from '@nestjs/common';
import { TimeDwService } from './time_dw.service';
import { TimeDwController } from './time_dw.controller';
import { TimeDw } from './entities/time_dw.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dashboard } from 'src/dashboard/entities/dashboard.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TimeDw, Dashboard])],
  controllers: [TimeDwController],
  providers: [TimeDwService],
})
export class TimeDwModule {}
