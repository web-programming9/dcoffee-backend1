import { Dashboard } from 'src/dashboard/entities/dashboard.entity';
import { Column, OneToMany, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TimeDw {
  @PrimaryGeneratedColumn()
  Time_Key: number;

  @Column()
  Time_DW_Year: number;

  @Column()
  Time_DW_Quarter: number;

  @Column()
  Time_DW_Month: number;

  @Column()
  Time_DW_Date: string;

  @Column()
  Time_DW_DateOfWeek: string;

  @Column()
  Time_DW_Season: string;

  @Column()
  Time_DW_Hour: number;

  @OneToMany(() => Dashboard, (Dashboard) => Dashboard.Time_Key)
  Dashboard: Dashboard;
}
