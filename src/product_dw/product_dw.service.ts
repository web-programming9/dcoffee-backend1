import { Injectable, NotFoundException } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDwDto } from './dto/create-product_dw.dto';
import { UpdateProductDwDto } from './dto/update-product_dw.dto';
import { ProductDw } from './entities/product_dw.entity';

@Injectable()
export class ProductDwService {
  constructor(
    @InjectRepository(ProductDw)
    private ProductDwsitory: Repository<ProductDw>,
  ) {}

  create(createProductDwDto: CreateProductDwDto) {
    return this.ProductDwsitory.save(createProductDwDto);
  }

  findAll() {
    return this.ProductDwsitory.find({});
  }

  findOne(Product_Key: number) {
    return this.ProductDwsitory.findOne({ where: { Product_Key } });
  }

  async update(Product_Key: number, updateProductDwDto: UpdateProductDwDto) {
    const productdw = await this.ProductDwsitory.findOneBy({ Product_Key });
    if (!productdw) {
      throw new NotFoundException();
    }
    const updatedproductdw = { ...productdw, ...updateProductDwDto };
    return this.ProductDwsitory.save(updatedproductdw);
  }

  async remove(Product_Key: number) {
    const productdw = await this.ProductDwsitory.findOne({
      where: { Product_Key: Product_Key },
    });
    try {
      const deletedProductDw = await this.ProductDwsitory.remove(productdw);
      return deletedProductDw;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
