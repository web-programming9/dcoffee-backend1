import { Test, TestingModule } from '@nestjs/testing';
import { ProductDwController } from './product_dw.controller';
import { ProductDwService } from './product_dw.service';

describe('ProductDwController', () => {
  let controller: ProductDwController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductDwController],
      providers: [ProductDwService],
    }).compile();

    controller = module.get<ProductDwController>(ProductDwController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
