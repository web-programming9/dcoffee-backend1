import { Module } from '@nestjs/common';
import { ProductDwService } from './product_dw.service';
import { ProductDwController } from './product_dw.controller';
import { ProductDw } from './entities/product_dw.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dashboard } from 'src/dashboard/entities/dashboard.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProductDw, Dashboard])],
  controllers: [ProductDwController],
  providers: [ProductDwService],
})
export class ProductDwModule {}
