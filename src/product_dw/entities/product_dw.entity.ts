import { Dashboard } from 'src/dashboard/entities/dashboard.entity';
import { PrimaryColumn, Column, OneToMany, Entity } from 'typeorm';

@Entity()
export class ProductDw {
  @PrimaryColumn()
  Product_Key: number;

  @Column()
  Product_DW_Name: string;

  @Column()
  Product_DW_Catagory: string;

  @Column()
  Product_DW_SubCategory: string;

  @Column()
  Product_DW_Size: string;

  @Column()
  Product_DW_SellPrice: number;

  @OneToMany(() => Dashboard, (Dashboard) => Dashboard.Product_Key)
  Dashboard: Dashboard;
}
