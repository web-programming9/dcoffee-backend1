import { Module } from '@nestjs/common';
import { EmployeeService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from './entities/employee.entity';
import { User } from 'src/users/entities/user.entity';
import { Order } from 'src/orders/entities/order.entity';
import { CheckinOut } from 'src/checkin-out/entities/checkin-out.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee, User, Order, CheckinOut])],
  controllers: [EmployeesController],
  providers: [EmployeeService],
})
export class EmployeesModule {}
