import { Bill } from 'src/bills/entities/bill.entity';
import { CheckMaterial } from 'src/check_materials/entities/check_material.entity';
import { CheckinOut } from 'src/checkin-out/entities/checkin-out.entity';
import { Order } from 'src/orders/entities/order.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '50',
  })
  name: string;

  @Column({
    length: '150',
  })
  address: string;

  @Column({
    unique: true,
    length: '10',
  })
  tel: string;

  @Column({
    unique: true,
    length: '64',
  })
  email: string;

  @Column({
    length: '32',
  })
  position: string;

  @Column({
    type: 'float',
  })
  hourly_wage: number;

  @CreateDateColumn()
  start_date: Date;

  @OneToMany(() => Order, (order) => order.employee)
  orders: Order[];

  @OneToMany(() => CheckinOut, (checkinOut) => checkinOut.employee)
  checkinOut: CheckinOut;

  @OneToMany(() => Bill, (bills) => bills.employee)
  bills: Bill;

  @OneToOne(() => User, (user) => user.employee)
  @JoinColumn()
  user: User;

  @OneToMany(() => CheckMaterial, (check_material) => check_material.employeeId)
  check_material: CheckMaterial;

  @UpdateDateColumn()
  updatedAt: Date;
}
