import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private EmployeesRepository: Repository<Employee>,
  ) {}
  create(createEmployeeDto: CreateEmployeeDto) {
    return this.EmployeesRepository.save(createEmployeeDto);
  }

  findAll() {
    return this.EmployeesRepository.find({
      relations: ['user'],
    });
  }

  findOne(id: number) {
    return this.EmployeesRepository.findOne({ where: { id } });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const employee = await this.EmployeesRepository.findOneBy({ id });
    if (!employee) {
      throw new NotFoundException();
    }
    const updateEmployee = { ...employee, ...updateEmployeeDto };
    return this.EmployeesRepository.save(updateEmployee);
  }

  async remove(id: number) {
    const Employee = await this.EmployeesRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedUser = await this.EmployeesRepository.remove(Employee);
      return deletedUser;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
