import { Test, TestingModule } from '@nestjs/testing';
import { CustomerDwService } from './customer_dw.service';

describe('CustomerDwService', () => {
  let service: CustomerDwService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CustomerDwService],
    }).compile();

    service = module.get<CustomerDwService>(CustomerDwService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
