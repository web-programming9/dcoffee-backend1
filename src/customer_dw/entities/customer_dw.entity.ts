import { Dashboard } from 'src/dashboard/entities/dashboard.entity';
import { Column, OneToMany, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class CustomerDw {
  @PrimaryColumn()
  Customer_Key: number;

  @Column()
  Customer_DW_Name: string;

  @Column()
  Customer_DW_StartDate: Date;

  @OneToMany(() => Dashboard, (Dashboard) => Dashboard.Customer_Key)
  Dashboard: Dashboard;
}
