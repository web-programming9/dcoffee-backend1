import { Test, TestingModule } from '@nestjs/testing';
import { CustomerDwController } from './customer_dw.controller';
import { CustomerDwService } from './customer_dw.service';

describe('CustomerDwController', () => {
  let controller: CustomerDwController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CustomerDwController],
      providers: [CustomerDwService],
    }).compile();

    controller = module.get<CustomerDwController>(CustomerDwController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
