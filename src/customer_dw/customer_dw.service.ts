import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCustomerDwDto } from './dto/create-customer_dw.dto';
import { UpdateCustomerDwDto } from './dto/update-customer_dw.dto';
import { CustomerDw } from './entities/customer_dw.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CustomerDwService {
  constructor(
    @InjectRepository(CustomerDw)
    private CustomerDwsitory: Repository<CustomerDw>,
  ) {}

  create(createCustomerDwDto: CreateCustomerDwDto) {
    return this.CustomerDwsitory.save(createCustomerDwDto);
  }

  findAll() {
    return this.CustomerDwsitory.find({});
  }

  findOne(Customer_Key: number) {
    return this.CustomerDwsitory.findOne({ where: { Customer_Key } });
  }

  async update(Customer_Key: number, updateCustomerDwDto: UpdateCustomerDwDto) {
    const CustomerDw = await this.CustomerDwsitory.findOneBy({ Customer_Key });
    if (!CustomerDw) {
      throw new NotFoundException();
    }
    const updatedCustomerDw = { ...CustomerDw, ...updateCustomerDwDto };
    return this.CustomerDwsitory.save(updatedCustomerDw);
  }

  async remove(Customer_Key: number) {
    const CustomerDw = await this.CustomerDwsitory.findOne({
      where: { Customer_Key: Customer_Key },
    });
    try {
      const deletedCustomerDw = await this.CustomerDwsitory.remove(CustomerDw);
      return deletedCustomerDw;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
