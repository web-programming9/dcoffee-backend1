import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CustomerDwService } from './customer_dw.service';
import { CreateCustomerDwDto } from './dto/create-customer_dw.dto';
import { UpdateCustomerDwDto } from './dto/update-customer_dw.dto';

@Controller('customer-dw')
export class CustomerDwController {
  constructor(private readonly customerDwService: CustomerDwService) {}

  @Post()
  create(@Body() createCustomerDwDto: CreateCustomerDwDto) {
    return this.customerDwService.create(createCustomerDwDto);
  }

  @Get()
  findAll() {
    return this.customerDwService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.customerDwService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCustomerDwDto: UpdateCustomerDwDto,
  ) {
    return this.customerDwService.update(+id, updateCustomerDwDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.customerDwService.remove(+id);
  }
}
