import { PartialType } from '@nestjs/mapped-types';
import { CreateCustomerDwDto } from './create-customer_dw.dto';

export class UpdateCustomerDwDto extends PartialType(CreateCustomerDwDto) {}
