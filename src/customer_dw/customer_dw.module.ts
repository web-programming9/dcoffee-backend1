import { Module } from '@nestjs/common';
import { CustomerDwService } from './customer_dw.service';
import { CustomerDwController } from './customer_dw.controller';
import { CustomerDw } from './entities/customer_dw.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dashboard } from 'src/dashboard/entities/dashboard.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CustomerDw, Dashboard])],
  controllers: [CustomerDwController],
  providers: [CustomerDwService],
})
export class CustomerDwModule {}
