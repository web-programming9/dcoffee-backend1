import { Bill } from 'src/bills/entities/bill.entity';
import { Material } from 'src/materials/entities/material.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Entity,
} from 'typeorm';

@Entity()
export class BillDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({ default: 0 })
  amount: number;

  @Column({ type: 'float' })
  price: number;

  @Column({ type: 'float' })
  total: number;

  @ManyToOne(() => Bill, (bill) => bill.bill_details)
  bill: Bill;

  @ManyToOne(() => Material, (material) => material.billDetail)
  material: Material;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
