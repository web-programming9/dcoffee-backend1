import { IsNotEmpty, Length, Min } from 'class-validator';
export class CreateBillDetailDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  @Min(0)
  amount: number;

  @IsNotEmpty()
  @Min(0)
  price: number;

  @IsNotEmpty()
  @Min(0)
  total: number;
}
