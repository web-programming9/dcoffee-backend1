import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBillDetailDto } from './dto/create-bill_detail.dto';
import { UpdateBillDetailDto } from './dto/update-bill_detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { BillDetail } from './entities/bill_detail.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BillDetailsService {
  constructor(
    @InjectRepository(BillDetail)
    private BillDetailsRepository: Repository<BillDetail>,
  ) {}

  create(createBillDetailDto: CreateBillDetailDto) {
    return this.BillDetailsRepository.save(createBillDetailDto);
  }

  findAll() {
    return this.BillDetailsRepository.find({});
  }

  async findOne(id: number) {
    const BillDetail = await this.BillDetailsRepository.findOne({
      where: { id: id },
      relations: ['bill'],
    });
    if (!BillDetail) {
      throw new NotFoundException();
    }
    return BillDetail;
  }

  async update(id: number, updateBillDetailDto: UpdateBillDetailDto) {
    const BillDetail = await this.BillDetailsRepository.findOneBy({
      id: id,
    });
    if (!BillDetail) {
      throw new NotFoundException();
    }
    const updatedBillDetail = { ...BillDetail, ...updateBillDetailDto };
    return this.BillDetailsRepository.save(updatedBillDetail);
  }

  async remove(id: number) {
    const BillDetail = await this.BillDetailsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedBillDetail = await this.BillDetailsRepository.remove(
        BillDetail,
      );
      return deletedBillDetail;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
