import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/order-item';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { StoresModule } from './stores/stores.module';
import { Store } from './stores/entities/store.entity';
import { CatagorysModule } from './catagorys/catagorys.module';
import { Catagory } from './catagorys/entities/catagory.entity';
import { ReportsModule } from './reports/reports.module';
import { MaterialsModule } from './materials/materials.module';
import { Material } from './materials/entities/material.entity';
import { CheckinOutModule } from './checkin-out/checkin-out.module';
import { SummarySalarysModule } from './summary_salarys/summary_salarys.module';
import { SummarySalary } from './summary_salarys/entities/summary_salary.entity';
import { CheckinOut } from './checkin-out/entities/checkin-out.entity';
import { BillsModule } from './bills/bills.module';
import { Bill } from './bills/entities/bill.entity';
import { BillDetailsModule } from './bill_details/bill_details.module';
import { BillDetail } from './bill_details/entities/bill_detail.entity';
import { CheckMaterialsModule } from './check_materials/check_materials.module';
import { CheckMaterialDetailsModule } from './check-material-details/check-material-details.module';
import { CheckMaterialDetail } from './check-material-details/entities/check-material-detail.entity';
import { CheckMaterial } from './check_materials/entities/check_material.entity';
import { CustomerDwModule } from './customer_dw/customer_dw.module';
import { EmployeeDwModule } from './employee_dw/employee_dw.module';
import { ProductDwModule } from './product_dw/product_dw.module';
import { StoreDwModule } from './store_dw/store_dw.module';
import { TimeDwModule } from './time_dw/time_dw.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { Dashboard } from './dashboard/entities/dashboard.entity';
import { StoreDw } from './store_dw/entities/store_dw.entity';
import { TimeDw } from './time_dw/entities/time_dw.entity';
import { ProductDw } from './product_dw/entities/product_dw.entity';
import { EmployeeDw } from './employee_dw/entities/employee_dw.entity';
import { CustomerDw } from './customer_dw/entities/customer_dw.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot(
      // {
      //   type: 'sqlite',
      //   database: 'db.sqlite',
      //   synchronize: true,
      //   migrations: [],
      //   entities: [
      //     Customer,
      //     Product,
      //     Order,
      //     OrderItem,
      //     User,
      //     Employee,
      //     Store,
      //     Catagory,
      //     SummarySalary,
      //     CheckinOut,
      //     Bill,
      //     BillDetail,
      //     CheckMaterialDetail,
      //     CheckMaterial,
      //     Material,
      //     Dashboard,
      //     StoreDw,
      //     TimeDw,
      //     ProductDw,
      //     EmployeeDw,
      //     CustomerDw,
      //   ],
      // },
      {
        type: 'mysql',
        host: 'db4free.net',
        port: 3306,
        username: 'obob_coffee',
        password: 'Pass@1234',
        database: 'obob_coffee_db',
        entities: [
          Customer,
          Product,
          Order,
          OrderItem,
          User,
          Employee,
          Store,
          Catagory,
          SummarySalary,
          CheckinOut,
          Bill,
          BillDetail,
          CheckMaterialDetail,
          CheckMaterial,
          Material,
          Dashboard,
          StoreDw,
          TimeDw,
          ProductDw,
          EmployeeDw,
          CustomerDw,
        ],
        synchronize: true,
      },
    ),

    CustomersModule,
    ProductsModule,
    OrdersModule,
    UsersModule,
    AuthModule,
    EmployeesModule,
    StoresModule,
    CatagorysModule,
    ReportsModule,
    MaterialsModule,
    CheckinOutModule,
    SummarySalarysModule,
    BillsModule,
    BillDetailsModule,
    CheckMaterialsModule,
    CheckMaterialDetailsModule,
    ReportsModule,
    CustomerDwModule,
    EmployeeDwModule,
    ProductDwModule,
    StoreDwModule,
    TimeDwModule,
    DashboardModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
