import { CheckMaterialDetail } from 'src/check-material-details/entities/check-material-detail.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class CheckMaterial {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  mat_date_time: Date;

  @ManyToOne(() => Employee, (employee) => employee.check_material)
  employeeId: Employee;

  @OneToMany(
    () => CheckMaterialDetail,
    (check_material_detail) => check_material_detail.check_material,
  )
  check_material_detail: CheckMaterialDetail;

  @UpdateDateColumn()
  updatedAt: Date;
}
