import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckMaterialDto } from './dto/create-check_material.dto';
import { UpdateCheckMaterialDto } from './dto/update-check_material.dto';
import { CheckMaterial } from './entities/check_material.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class CheckMaterialsService {
  constructor(
    @InjectRepository(CheckMaterial)
    private checkMaterialsRepository: Repository<CheckMaterial>,
  ) {}
  create(createCheckMaterialDto: CreateCheckMaterialDto) {
    return this.checkMaterialsRepository.save(createCheckMaterialDto);
  }

  findAll() {
    return this.checkMaterialsRepository.find({});
  }

  findOne(id: number) {
    return this.checkMaterialsRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateCheckMaterialDto: UpdateCheckMaterialDto) {
    const checkMaterial = await this.checkMaterialsRepository.findOneBy({
      id,
    });
    if (!checkMaterial) {
      throw new NotFoundException();
    }
    const updateCheckMaterial = { ...checkMaterial, ...updateCheckMaterialDto };
    return this.checkMaterialsRepository.save(updateCheckMaterial);
  }

  async remove(id: number) {
    const checkMaterial = await this.checkMaterialsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedcheckMaterial = await this.checkMaterialsRepository.remove(
        checkMaterial,
      );
      return deletedcheckMaterial;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
