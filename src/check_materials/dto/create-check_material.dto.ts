import { IsNotEmpty } from 'class-validator';

export class CreateCheckMaterialDto {
  @IsNotEmpty()
  mat_date_time: Date;
}
