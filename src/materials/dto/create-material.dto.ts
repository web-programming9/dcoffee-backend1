import { IsNotEmpty, Length } from 'class-validator';

export class CreateMaterialDto {
  @IsNotEmpty()
  @Length(3, 50)
  name: string;

  @IsNotEmpty()
  min_quantity: number;

  @IsNotEmpty()
  quantity: number;

  @IsNotEmpty()
  unit: string;

  @IsNotEmpty()
  price_per_unit: number;
}
