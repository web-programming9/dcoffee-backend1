import { BillDetail } from 'src/bill_details/entities/bill_detail.entity';
import { CheckMaterialDetail } from 'src/check-material-details/entities/check-material-detail.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '50',
  })
  name: string;

  @Column({
    default: 0,
  })
  min_quantity: number;

  @Column({
    default: 0,
  })
  quantity: number;

  @Column({
    default: '-',
  })
  unit: string;

  @Column({
    default: 0,
    type: 'float',
  })
  price_per_unit: number;

  @OneToMany(
    () => CheckMaterialDetail,
    (check_material_detail) => check_material_detail.material,
  )
  check_material_detail: CheckMaterialDetail;

  @OneToMany(() => BillDetail, (billDetail) => billDetail.material)
  billDetail: BillDetail;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
