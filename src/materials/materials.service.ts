import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialsService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}

  async create(createMaterialDto: CreateMaterialDto) {
    return this.materialsRepository.save(createMaterialDto);
  }

  findAll() {
    return this.materialsRepository.find({});
  }

  findOne(id: number) {
    return this.materialsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.materialsRepository.findOneBy({
      id: id,
    });
    if (!material) {
      throw new NotFoundException();
    }
    const updateMateria = { ...material, ...updateMaterialDto };
    return this.materialsRepository.save(updateMateria);
  }

  async remove(id: number) {
    const material = await this.materialsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedmaterial = await this.materialsRepository.remove(material);
      return deletedmaterial;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
