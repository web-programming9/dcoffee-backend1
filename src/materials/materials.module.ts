import { Module } from '@nestjs/common';
import { MaterialsService } from './materials.service';
import { MaterialsController } from './materials.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Material } from './entities/material.entity';
import { BillDetail } from 'src/bill_details/entities/bill_detail.entity';
import { CheckMaterialDetail } from 'src/check-material-details/entities/check-material-detail.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Material, BillDetail, CheckMaterialDetail]),
  ],
  controllers: [MaterialsController],
  providers: [MaterialsService],
})
export class MaterialsModule {}
