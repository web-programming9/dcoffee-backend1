import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateDashboardDto } from './dto/create-dashboard.dto';
import { UpdateDashboardDto } from './dto/update-dashboard.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Dashboard } from './entities/dashboard.entity';

@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(Dashboard)
    private Dashboardsitory: Repository<Dashboard>,
  ) {}

  create(createDashboardDto: CreateDashboardDto) {
    return this.Dashboardsitory.save(createDashboardDto);
  }

  findAll() {
    return this.Dashboardsitory.find({
      relations: [
        'Time_Key',
        'Product_Key',
        'Employee_Key',
        'Customer_Key',
        'Store_Key',
      ],
    });
  }

  findOne(Dashboard_id: number) {
    return this.Dashboardsitory.findOne({ where: { Dashboard_id } });
  }

  async update(Dashboard_id: number, updateDashboardDto: UpdateDashboardDto) {
    const Dashboard = await this.Dashboardsitory.findOneBy({ Dashboard_id });
    if (!Dashboard) {
      throw new NotFoundException();
    }
    const updatedDashboard = { ...Dashboard, ...updateDashboardDto };
    return this.Dashboardsitory.save(updatedDashboard);
  }

  async remove(Dashboard_id: number) {
    const Dashboard = await this.Dashboardsitory.findOne({
      where: { Dashboard_id: Dashboard_id },
    });
    try {
      const deletedDashboard = await this.Dashboardsitory.remove(Dashboard);
      return deletedDashboard;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
