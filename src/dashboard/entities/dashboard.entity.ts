import { CustomerDw } from 'src/customer_dw/entities/customer_dw.entity';
import { EmployeeDw } from 'src/employee_dw/entities/employee_dw.entity';
import { ProductDw } from 'src/product_dw/entities/product_dw.entity';
import { StoreDw } from 'src/store_dw/entities/store_dw.entity';
import { TimeDw } from 'src/time_dw/entities/time_dw.entity';
import { PrimaryGeneratedColumn, Column, Entity, ManyToOne } from 'typeorm';

@Entity()
export class Dashboard {
  @PrimaryGeneratedColumn()
  Dashboard_id: number;

  @Column({ type: 'float' })
  Total_of_Sale: number;

  @Column({ type: 'float' })
  Amount_of_Sale: number;

  @Column({ type: 'float' })
  Discount_of_Sale: number;

  @ManyToOne(() => TimeDw, (timedw) => timedw.Dashboard)
  Time_Key: TimeDw;

  @ManyToOne(() => ProductDw, (ProductDw) => ProductDw.Dashboard)
  Product_Key: ProductDw;

  @ManyToOne(() => EmployeeDw, (EmployeeDw) => EmployeeDw.Dashboard)
  Employee_Key: EmployeeDw;

  @ManyToOne(() => CustomerDw, (CustomerDw) => CustomerDw.Dashboard)
  Customer_Key: CustomerDw;

  @ManyToOne(() => StoreDw, (StoreDw) => StoreDw.Dashboard)
  Store_Key: StoreDw;
}
