import { Module } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardController } from './dashboard.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dashboard } from './entities/dashboard.entity';
import { CustomerDw } from 'src/customer_dw/entities/customer_dw.entity';
import { EmployeeDw } from 'src/employee_dw/entities/employee_dw.entity';
import { ProductDw } from 'src/product_dw/entities/product_dw.entity';
import { StoreDw } from 'src/store_dw/entities/store_dw.entity';
import { TimeDw } from 'src/time_dw/entities/time_dw.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Dashboard,
      CustomerDw,
      EmployeeDw,
      ProductDw,
      StoreDw,
      TimeDw,
    ]),
  ],
  controllers: [DashboardController],
  providers: [DashboardService],
})
export class DashboardModule {}
