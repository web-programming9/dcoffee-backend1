import { Customer } from 'src/customers/entities/customer.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Store } from 'src/stores/entities/store.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { OrderItem } from './order-item';
@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  order_date_time: Date;

  @Column({
    default: 0,
    type: 'float',
  })
  discount: number;

  @Column({ type: 'float' })
  total: number;

  @Column({ type: 'float', default: 0 })
  received: number;

  @Column({ type: 'float' })
  change: number;

  @Column({
    length: '32',
    default: '-',
  })
  payment: string;

  @Column()
  amount: number;

  @Column({ default: '-' })
  tel: string;

  @ManyToOne(() => Store, (sotre) => sotre.orders)
  store: Store; // Store Id

  @ManyToOne(() => Employee, (employee) => employee.orders)
  employee: Employee; // Employee Id

  @ManyToOne(() => Customer, (customer) => customer.orders)
  customer: Customer; // Customer Id

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];

  @UpdateDateColumn()
  updatedDate: Date;
}
