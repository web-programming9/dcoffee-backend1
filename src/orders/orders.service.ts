import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Product } from 'src/products/entities/product.entity';
import { Store } from 'src/stores/entities/store.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';
import { UpdateOrderDto } from './dto/update-order.dto';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Store)
    private storesRepository: Repository<Store>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    const order: Order = new Order();
    console.log(createOrderDto);
    const customer = await this.customersRepository.findOneBy({
      tel: createOrderDto.tel,
    });
    const store = await this.storesRepository.findOneBy({
      id: createOrderDto.storeId,
    });
    const employee = await this.employeesRepository.findOneBy({
      id: createOrderDto.employeeId,
    });

    if (createOrderDto.tel) {
      const customer = await this.customersRepository.findOneBy({
        tel: createOrderDto.tel,
      });
      order.customer = customer;
      order.tel = customer.tel;
    } else {
      order.customer = null;
    }

    order.store = store;
    order.employee = employee;
    order.amount = 0;
    order.discount = createOrderDto.discount;
    order.total = 0;
    order.change = 0;
    order.received = createOrderDto.received;
    order.payment = createOrderDto.payment;
    await this.ordersRepository.save(order); // ได้ id
    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = od.amount;
      orderItem.product = await this.productsRepository.findOneBy({
        id: od.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.total = orderItem.price * orderItem.amount;
      orderItem.order = order; // อ้างกลับ
      await this.orderItemsRepository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
      order.total = order.total - order.discount;
      order.change = order.received - order.total;
    }
    await this.ordersRepository.save(order); // ได้ id
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['customer', 'employee', 'store', 'orderItems'],
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['customer', 'employee', 'store', 'orderItems'],
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    const order = await this.ordersRepository.findOneBy({
      id: id,
    });
    if (!order) {
      throw new NotFoundException();
    }
    const updateMateria = { ...order, ...updateOrderDto };
    return this.ordersRepository.save(updateMateria);
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedorder = await this.ordersRepository.remove(order);
      return deletedorder;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
