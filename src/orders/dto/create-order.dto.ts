import { IsNotEmpty } from 'class-validator';

class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;

  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
  // queue: number;
  order_date_time: Date;
  discount: number;
  total: number;
  received: number;
  change: number;
  // @Length(0, 32)
  payment: string;
  // @IsNotEmpty()
  storeId: number;
  // @IsNotEmpty()
  employeeId: number;
  tel: string;
  customerId: number;
}
