import { IsNotEmpty } from 'class-validator';

export class CreateCheckMaterialDetailDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  cmd_qty_lats: number;

  @IsNotEmpty()
  cmd_qty_remain: number;

  @IsNotEmpty()
  cmd_qty_expire: number;
}
