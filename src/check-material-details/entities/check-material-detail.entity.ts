import { CheckMaterial } from 'src/check_materials/entities/check_material.entity';
import { Material } from 'src/materials/entities/material.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CheckMaterialDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  cmd_name: string;

  @Column({ default: 0 })
  cmd_qty_last: number;

  @Column({ default: 0 })
  cmd_qty_remain: number;

  @Column({ default: 0, type: 'float' })
  cmd_qty_expire: number;

  @ManyToOne(
    () => CheckMaterial,
    (check_material) => check_material.check_material_detail,
  )
  check_material: CheckMaterial;

  @ManyToOne(() => Material, (material) => material.check_material_detail)
  material: Material;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
