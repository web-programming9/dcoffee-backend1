import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckMaterialDetailDto } from './dto/create-check-material-detail.dto';
import { UpdateCheckMaterialDetailDto } from './dto/update-check-material-detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CheckMaterialDetail } from './entities/check-material-detail.entity';

@Injectable()
export class CheckMaterialDetailsService {
  constructor(
    @InjectRepository(CheckMaterialDetail)
    private checkMaterialDetailsRepository: Repository<CheckMaterialDetail>,
  ) {}
  create(createCheckMaterialDetailsDto: CreateCheckMaterialDetailDto) {
    return this.checkMaterialDetailsRepository.save(
      createCheckMaterialDetailsDto,
    );
  }

  findAll() {
    return this.checkMaterialDetailsRepository.find({});
  }

  findOne(id: number) {
    return this.checkMaterialDetailsRepository.findOne({
      where: { id },
    });
  }

  async update(
    id: number,
    updateCheckMaterialDetailDto: UpdateCheckMaterialDetailDto,
  ) {
    const check_material_detail =
      await this.checkMaterialDetailsRepository.findOneBy({
        id,
      });
    if (!check_material_detail) {
      throw new NotFoundException();
    }
    const updateCheckMaterialDetail = {
      ...check_material_detail,
      ...updateCheckMaterialDetailDto,
    };
    return this.checkMaterialDetailsRepository.save(updateCheckMaterialDetail);
  }

  async remove(id: number) {
    const check_material = await this.checkMaterialDetailsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedcheck_material =
        await this.checkMaterialDetailsRepository.remove(check_material);
      return deletedcheck_material;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
