import { Module } from '@nestjs/common';
import { CheckMaterialDetailsService } from './check-material-details.service';
import { CheckMaterialDetailsController } from './check-material-details.controller';
import { CheckMaterialDetail } from './entities/check-material-detail.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckMaterial } from 'src/check_materials/entities/check_material.entity';
import { Material } from 'src/materials/entities/material.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([CheckMaterialDetail, CheckMaterial, Material]),
  ],
  controllers: [CheckMaterialDetailsController],
  providers: [CheckMaterialDetailsService],
})
export class CheckMaterialDetailsModule {}
