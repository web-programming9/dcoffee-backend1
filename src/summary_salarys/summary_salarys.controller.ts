import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { SummarySalarysService } from './summary_salarys.service';
import { CreateSummarySalaryDto } from './dto/create-summary_salary.dto';
import { UpdateSummarySalaryDto } from './dto/update-summary_salary.dto';

@Controller('summary-salarys')
export class SummarySalarysController {
  constructor(private readonly summarySalarysService: SummarySalarysService) {}

  @Post()
  create(@Body() createSummarySalaryDto: CreateSummarySalaryDto) {
    return this.summarySalarysService.create(createSummarySalaryDto);
  }

  @Get()
  findAll() {
    return this.summarySalarysService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.summarySalarysService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateSummarySalaryDto: UpdateSummarySalaryDto,
  ) {
    return this.summarySalarysService.update(+id, updateSummarySalaryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.summarySalarysService.remove(+id);
  }
}
