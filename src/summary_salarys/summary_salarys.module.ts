import { Module } from '@nestjs/common';
import { SummarySalarysService } from './summary_salarys.service';
import { SummarySalarysController } from './summary_salarys.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SummarySalary } from './entities/summary_salary.entity';
import { CheckinOut } from 'src/checkin-out/entities/checkin-out.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SummarySalary, CheckinOut])],
  controllers: [SummarySalarysController],
  providers: [SummarySalarysService],
})
export class SummarySalarysModule {}
