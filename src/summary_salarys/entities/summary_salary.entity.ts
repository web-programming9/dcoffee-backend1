import { CheckinOut } from 'src/checkin-out/entities/checkin-out.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class SummarySalary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @Column()
  work_hour: number;

  @Column({
    default: 0,
    type: 'float',
  })
  salary: number;

  @OneToMany(() => CheckinOut, (checkinOut) => checkinOut.summary)
  checkinOut: CheckinOut;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
