import { Min } from 'class-validator';

export class CreateSummarySalaryDto {
  @Min(0)
  salary: number;
}
