import { Test, TestingModule } from '@nestjs/testing';
import { SummarySalarysController } from './summary_salarys.controller';
import { SummarySalarysService } from './summary_salarys.service';

describe('SummarySalarysController', () => {
  let controller: SummarySalarysController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SummarySalarysController],
      providers: [SummarySalarysService],
    }).compile();

    controller = module.get<SummarySalarysController>(SummarySalarysController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
