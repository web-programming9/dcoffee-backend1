import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSummarySalaryDto } from './dto/create-summary_salary.dto';
import { UpdateSummarySalaryDto } from './dto/update-summary_salary.dto';
import { SummarySalary } from './entities/summary_salary.entity';

@Injectable()
export class SummarySalarysService {
  constructor(
    @InjectRepository(SummarySalary)
    private summarySalarysRepository: Repository<SummarySalary>,
  ) {}

  async create(createSummarySalaryDto: CreateSummarySalaryDto) {
    return this.summarySalarysRepository.save(createSummarySalaryDto);
  }

  findAll() {
    return this.summarySalarysRepository.find({});
  }

  findOne(id: number) {
    return this.summarySalarysRepository.findOne({
      where: { id: id },
    });
  }

  async update(id: number, updateSummarySalaryDto: UpdateSummarySalaryDto) {
    const summarySalary = await this.summarySalarysRepository.findOneBy({
      id: id,
    });
    if (!summarySalary) {
      throw new NotFoundException();
    }
    const updateSummarySalary = { ...summarySalary, ...updateSummarySalaryDto };
    return this.summarySalarysRepository.save(updateSummarySalary);
  }

  async remove(id: number) {
    const summarySalary = await this.summarySalarysRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedsummarySalary = await this.summarySalarysRepository.remove(
        summarySalary,
      );
      return deletedsummarySalary;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
