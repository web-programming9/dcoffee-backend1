import { Test, TestingModule } from '@nestjs/testing';
import { SummarySalarysService } from './summary_salarys.service';

describe('SummarySalarysService', () => {
  let service: SummarySalarysService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SummarySalarysService],
    }).compile();

    service = module.get<SummarySalarysService>(SummarySalarysService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
